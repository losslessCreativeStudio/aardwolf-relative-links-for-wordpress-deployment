<?php
   /*
   Plugin Name: Aardwolf Relative Links for WordPress Deployment
   Plugin URI: 
   Description: Allows any WordPress website to have the best of both worlds when it comes to relative links and SEO on different deployment environments. Simply add %HOME% to the root of links (in pages, posts, widgets, style.css, menu items ) and the plugin will replace that with your home URL respectively. This means no more replacing links when you change domains, or development environments!
   Version: 2.2
   Author: Miles Fonda
   Author URI: 
   License: GPL2
   */

//Globals
$homeURL = get_home_url();

/******************************************
*
* INCLUDE ADMIN OPTION PANEL
*
******************************************/

add_action('admin_menu', 'awrl4wd_menu');

function awrl4wd_menu() {
	add_options_page('Aardwolf Options', 'Aardworlf Links', 'manage_options', 'awrl4wd_options', 'awrl4wd_options_page');
}

function awrl4wd_options_page() {
	if ( !current_user_can( 'manage_options' ) ) {
		wp_die(_('You do not have sufficient permissions to access this page.') );
	}		
	include __DIR__."/awrl4wd_options.php";
}


/******************************************
*
* Filters html buffer (posts/pages)
*
******************************************/

function callback($buffer) {
   
    $newContent = str_replace("%HOME%", $homeURL , $buffer);
    
    $buffer = $newContent;
    return $buffer;
}

function buffer_start() {
  ob_start("callback");
}

function buffer_end() {
  ob_end_flush();
}

add_action('wp_head', 'buffer_start');
add_action('wp_footer', 'buffer_end');


/******************************************
*
* Filters styles and outputs to a new stylesheet
*
******************************************/

//only works with style.css of current theme
function awrl4wd_override_styles() {
	
	$stylesheet = get_stylesheet_uri();
    $styles = file_get_contents($stylesheet);
    $newStyles = str_replace("%HOME%", $homeURL , $styles);

    $altPath = plugins_url( 'override/style.css' , __FILE__ );
    
    $writeFileURL = WP_PLUGIN_DIR."/aardwolf-links/override/style.css";
    
    //put modified styles into the plugin stylesheet
    file_put_contents( $writeFileURL, $newStyles, FILE_APPEND);
    //dequeue the stylesheet
 	wp_dequeue_style('style'); 
 	//enqueue modified styles in plugin stylesheet
 	wp_enqueue_style('awrl4wd_styles', $altPath);  
}
add_action('wp_head', 'awrl4wd_override_styles');


/******************************************
*
* Change WordPress Editor Links to use %HOME% and %SITE%
*
******************************************/


function awrl4wd_full_urls($content) {
    $newContent = str_replace("%HOME%", $homeURL, $newContent);	
	
	$content = $newContent;
	return $content;
}

function awrl4wd_shorts($content) {
    $newContent = str_replace($homeURL , "%HOME%", $newContent);	
	
	$content = $newContent;
	return $content;
}

function awrl4wd_editor_links($html) {
    $newContent = str_replace($homeURL , "%HOME%", $newContent);	
	
	$html = $newContent;
	return $html;
}
add_filter('image_send_to_editor','awrl4wd_editor_links');
add_filter('image_send_to_editor_url','awrl4wd_editor_links');
add_filter('media_send_to_editor','awrl4wd_editor_links');


/******************************************
*
* Unpacks %HOME% and %SITE% to use url in editor visual view (Initializes Tiny MCE plugin)
*
******************************************/

class tinymce_awrl4wd_replace

{
	function __construct() {	
		add_filter('mce_external_plugins', array( &$this, 'add_tcustom_tinymce_plugin' ));
	}
    function add_tcustom_tinymce_plugin($plugin_array) {
       $plugin_array['awrl4wd'] = WP_PLUGIN_URL.'/aardwolf-links/tinymce-plugin/awrl4wd/editor_plugin.js';
        return $plugin_array;
    }
}
add_action("init", create_function('', 'new tinymce_awrl4wd_replace();'));

//Allows home and site variables to be passed to Tiny MCE js
wp_register_script( 'editor_plugin', WP_PLUGIN_URL.'/aardwolf-links/tinymce-plugin/awrl4wd/editor_plugin.js' );
$translation_array = array( 'homeURL' => get_home_url() );
wp_localize_script( 'editor_plugin', 'u', $translation_array );
wp_enqueue_script( 'editor_plugin' );


/******************************************
*
* Detect Browser Class
*
******************************************/

add_filter('body_class','browser_body_class');
function browser_body_class($classes) {http://www.milesdfonda.com.php53-25.dfw1-2.websitetestlink.com/demos/wp-content/uploads/2014/07/Photo-on-2-6-14-at-2.23-PM.jpg
  global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

  if($is_lynx) $classes[] = 'lynx';
  elseif($is_gecko) $classes[] = 'gecko';
  elseif($is_opera) $classes[] = 'opera';
  elseif($is_NS4) $classes[] = 'ns4';
  elseif($is_safari) $classes[] = 'safari';
  elseif($is_chrome) $classes[] = 'chrome';
  elseif($is_IE) $classes[] = 'ie';
  else $classes[] = 'unknown';

  if($is_iphone) $classes[] = 'iphone';
  return $classes;
}

/******************************************
*
* Sets editor to go to text mode by default
*
******************************************/

add_filter( 'wp_default_editor', create_function( '', 'return "html";' ) );


/******************************************
*
* Deactivation Function
*
******************************************/

register_deactivation_hook( __FILE__, 'awrl4wd_deactivate' );

function awrl4wd_deactivate() {

		
}