<?php

$homeURL = get_home_url();


/******************************************
*
* Set Placeholders in CSS
*
******************************************/

$stylesheetURL = get_stylesheet_uri();
$stylesheet = file_get_contents( $stylesheetURL, NULL);
    
$stylesheet = str_replace( $homeURL, "%HOME%", $stylesheet );	  
    
file_put_contents( $stylesheetURL, $stylesheet );  
    
/******************************************
*
* Set Placeholders in Text Widgets
*
******************************************/
/*
function awrl4wd_text_widget_placeholders($text) {
 	$text = str_replace( $homeURL, "%HOME%", $text);
 	return $text;
}
add_filter('widget_text', 'awrl4wd_text_widget_placeholders'); 
 */   
    
/******************************************
*
* Set Placeholders in Menu Items
*
******************************************/
/*
function awrl4wd_menu_placeholders($args) {
 	$text = str_replace( $homeURL, "%HOME%", $text);
 	return $args;
}
add_filter('wp_nav_menu_args', 'awrl4wd_menu_placeholders')*/