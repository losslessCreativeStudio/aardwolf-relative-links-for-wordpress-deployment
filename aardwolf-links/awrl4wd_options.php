<div class="wrap">
<div class="awrl4wd-message">
<p>Instances of <span class="homeurl"><?php echo get_home_url(); ?></span> will be converted to <strong>%HOME%</strong></p>
<p>Instances of <strong>%HOME%</strong> will be converted to <span class="homeurl"><?php echo get_home_url(); ?></span></p>
</div> 
<?php screen_icon(); ?>
<h2>Aardwolf Relative Links Options</h2>
</div>

<!--OPTION FIELDS
 ---------------------------------------->
<div class="awrl4wd-left">
<fieldset>
	<form method="post" action"">
<h3>Flush the CSS</h3>
<p>CSS related links acting goofy? AWRL4WD uses a CSS cache to convert those placholders on the live site. Use this button to clear it.</p>
		<input type="button" class="button button-primary" name="awrl4wd_flush" value="Flush CSS Override" onclick="awrl4wd_css_flush();">        
<h3>Auto Rebuild Placeholders</h3> 
<p>Use these options to replace absolute links on your site to relative placeholders.</p>
<input type="button" class="button button-primary" name="awrl4wd_all_placeholders" value="Set All to Placeholders" onclick="awrl4wd_set_all_pl();">
        <ul class="button-in">
       <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_pl_menu" value="Rebuild Menus" onclick="awrl4wd_set_pl_menu();"></li>
         <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_pl_widget" value="Rebuild Text Widgets" onclick="awrl4wd_set_pl_widget();"></li>
         <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_pl_css" value="Rebuild style.css" onclick="awrl4wd_set_pl_css();"></li>
          <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_pl_post" value="Rebuild Pages/ Posts" onclick="awrl4wd_set_pl_post();"></li>
            </ul>
               
<h3>Auto Rebuild Absolute Links</h3>
<p>Use these options to rebuild absolute links in place of the placeholders.</p>
<input type="button" class="button button-primary" name="awrl4wd_all_links" value="Set All to Absolute Links" onclick="awrl4wd_set_all_ab();">
        <ul class="button-in">
        <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_ab_menu" value="Rebuild Menus" onclick="awrl4wd_set_ab_menu();"></li>
        <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_ab_widget" value="Rebuild Text Widgets" onclick="awrl4wd_set_ab_widget();"></li>
        <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_ab_css" value="Rebuild style.css" onclick="awrl4wd_set_ab_css();"></li>
         <li><input type="button" class="button-secondary submit-add-to-menu right" name="awrl4wd_ab_post" value="Rebuild Pages/ Posts" onclick="awrl4wd_set_ab_post();"></li>
        </ul>
        </form>
    </fieldset>

    </div>
    
<div class="awrl4wd-right">
    <h3>How this Works</h3>
    <img class="wolf" src="<?php echo plugins_url( "images/awrl4wd-aardwolf.jpg", __FILE__ ) ?>" />
    <p>Absolute linking in WordPress can be a huge when working with multiple deployment environment or if you need to move your WordPress site. Sure, you can find and replace your exported database, or create a deployment script, and other plugin developers have come up with some great solutions to find and replace URLs when the WordPress URls have changed... but wouldn't you just prefer that those URLs automatically changed themselves? <strong><em>ME TOO. That's why I developed Aardwolf Relative Linking for WordPress Development.</em></strong></p>
<p>
<h3>STEP 1: Find an absolute link</h3>
<strong>Example:</strong><br />
<span class="homeurl">http://mysite.com/awesome-page</span>
<br />
<br />
OR
<br />
<br />
<span class="homeurl">http://mysite.com/site/my_picture.jpg</span>
</p>
 
<p>
<h3>STEP 2: Kick the Absolute Path out!</h3>
<p>Use the placeholder %HOME% to replace the absolute path.</p>
<strong>Example:</strong><br />
<i>Hover over absolute path to see:</i><br />
<span class="homeurl"><i class="yellow"></i>/awesome-page</span><br />
<span class="homeurl"><i class="yellow"></i>/site/my_picture.jpg</span>
</p>

<p>
<h3>STEP 3: The site will look the same no matter what domain it is on!</h3>
<span class="site">LOCAL!<img src="<?php echo plugins_url( "images/website.jpg", __FILE__ ) ?>" /></span>
<span class="site">STAGING!<img src="<?php echo plugins_url( "images/website.jpg", __FILE__ ) ?>" /></span>
<span class="site">LIVE!<img src="<?php echo plugins_url( "images/website.jpg", __FILE__ ) ?>" /></span>
</p>

<p>
AWRL4WD Placeholder works in Pages, Posts, Widgets, style.css and menu items.
</p>
</div>

<!--OPTION FIELDS
 ---------------------------------------->


<!------STYLES---->  
<style>
.wolf {
    width:100%;   
}
    
.awrl4wd-message {
    background-color: #fff;
    border-left: 4px solid #7ad03a;
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1);
    width:100%;
}
    
.site {
    display: inline-block;
    margin-bottom: 10px;
    margin-right: 10px;
    width: 170px;
    font-weight: bold;
}
 
.yellow {
    background-color: yellow;
    color: #000;
    display: inline-block;
    height: 20px;
    padding: 0 7px;
    position: relative;
    top: 7px;
    width: 136px;
}
    
.yellow:hover {
    transition: width 400ms, background-color 400ms;
    width: 52px;
    cursor: pointer;
    background-color: lightgreen;
}
     
.yellow:before {
    color: #000;
    content: "http://mysite.com";
    height: 19px;
    left: 8px;
    position: absolute;
    top: 3px;
}   
    
.yellow:hover:before {
    content: "%HOME%";
    width:52px;
}
    
.awrl4wd-left {
    float: left;
    margin-right: calc(8% - 20px);
    width: 46%;
}
    
.awrl4wd-right {
    float: left;   
    width: 46%;
    margin-right:20px;
}
    
h3 {
    margin-top:35px;   
}

.homeurl {
    background-color: #999;
    border: 1px solid #fff;
    color: #fff;
    font-family: courier;
    margin: 0 4px;
    padding: 3px 11px;
} 
      
.button-in > li {
    float: left;
}
    
.button-in {
    display: block;
    height: 129px;
    max-width: 200px;
}

@media (max-width: 600px) {
	.awrl4wd-right, .awrl4wd-left {
		width: 100%;	
	}
	
	.awrl4wd-message {
		display: none;	
	}
}
</style>

 
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">    
<!------Flush CSS---->
function awrl4wd_css_flush()
{
    var r = confirm("This will clear the override CSS. Are you sure you want to do this?")
    if(r == true)
    {    	
        $.ajax({
          url: '<?php plugins_url( "awrl4wd_flush.php", __FILE__ ) ?>',
          data: {'file' : "override/style.css" },
          success: function () {
             alert('The override CSS has been cleared. Loading the site will rebuild it.');
          },
          error: function () {
             alert('Something went wrong, the override CSS was NOT cleared.');
             
          }
        });
    }
}
</script>