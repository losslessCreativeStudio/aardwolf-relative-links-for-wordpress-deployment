/*-----------------------------------/
*
* Adaptation of plugin by Simon Dunton
* @http://www.wpsites.co.uk/435-replacing-inserted-shortcodes-in-tinymce-with-a-graphic.php
*
\-----------------------------------*/

(function() {
	tinymce.create('tinymce.plugins.awrl4wd', {

		init : function(ed, url) {
			var t = this;

			t.url = url;
			
			var homeURL = u.homeURL;
			
			ed.onBeforeSetContent.add(function(ed, o) {
				o.content = t._do_awrl4wd(o.content);
			});
			
			ed.onExecCommand.add(function(ed, cmd) {
			    if (cmd ==='mceInsertContent'){
					tinyMCE.activeEditor.setContent( t._do_awrl4wd(tinyMCE.activeEditor.getContent()) );
				}
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = t._get_awrl4wd(o.content);
			});
		},

		_do_awrl4wd : function(co) {
            var coFilter = co.split('%HOME%').join(u.homeURL);	
            return coFilter;
		},
		
		_get_awrl4wd : function(co) {
            var coFilter = co.split(u.homeURL).join('%HOME%');	
            return coFilter;
		},

		getInfo : function() {
			return {
				longname : 'AWRL4WD Replace Placeholder',
				author : 'Miles Fonda',
				authorurl : 'http://www.milesdfonda.com',
				infourl : '',
				version : "1.0"
			};
		}
	});

	tinymce.PluginManager.add('awrl4wd', tinymce.plugins.awrl4wd);
})();
